﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CompMan
{
    class Program
    {
        static void Main(string[] args)
        {
            seedDatabase();
            showCompetitors();
            //deleteAllfromDB(); 
            //deleteCompetitors();

        }

        public static void seedDatabase()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                competitorDBContext.Database.EnsureCreated();
                Team t1 = new Team() { TeamName = "TSM" };
                Team t2 = new Team() { TeamName = "CLG" };

                Coach co1 = new Coach() { Name = "Peter Zhang", Team = t1 };
                Coach co2 = new Coach() { Name = "Zikz", Team = t2 };
                Coach co3 = new Coach() { Name = "Reapered", Id = 3 };
                Coach co4 = new Coach() { Name = "Christopher Montecristo Mykles", Id = 4 };

                Competitor c1 = new Competitor() { Name = "Doublelift", Age = 31, Coach = co2 };
                Competitor c2 = new Competitor() { Name = "Impact", Age = 22, Coach = co1 };
                Competitor c3 = new Competitor() { Name = "Zven", Age = 21, Coach = co4 };
                Competitor c4 = new Competitor() { Name = "Biofrost", Age = 23, Coach = co3 };
                Competitor c5 = new Competitor() { Name = "Svenskeren", Age = 22 };

                Skill s1 = new Skill() { SkillName = "Ahri" };
                Skill s2 = new Skill() { SkillName = "Alistar" };
                Skill s3 = new Skill() { SkillName = "Jarvan IV" };
                Skill s4 = new Skill() { SkillName = "Zac" };
                Skill s5 = new Skill() { SkillName = "Tristana" };
                Skill s6 = new Skill() { SkillName = "Sett" };
                Skill s7 = new Skill() { SkillName = "Lucian" };


                ChampionSkill cs1 = new ChampionSkill(c1, s7);
                ChampionSkill cs2 = new ChampionSkill(c1, s5);
                ChampionSkill cs3 = new ChampionSkill(c1, s6);
                ChampionSkill cs4 = new ChampionSkill(c2, s2);
                ChampionSkill cs5 = new ChampionSkill(c2, s1);
                ChampionSkill cs6 = new ChampionSkill(c3, s2);
                ChampionSkill cs7 = new ChampionSkill(c4, s2);
                ChampionSkill cs8 = new ChampionSkill(c4, s5);
                ChampionSkill cs9 = new ChampionSkill(c5, s3);
                ChampionSkill cs10 = new ChampionSkill(c5, s4);


                t1.Competitors.Add(c1);
                t1.Competitors.Add(c2);
                t1.Competitors.Add(c3);
                t2.Competitors.Add(c4);
                t2.Competitors.Add(c5);

                competitorDBContext.Add(cs1);
                competitorDBContext.Add(cs2);
                competitorDBContext.Add(cs3);
                competitorDBContext.Add(cs4);
                competitorDBContext.Add(cs5);
                competitorDBContext.Add(cs6);
                competitorDBContext.Add(cs7);
                competitorDBContext.Add(cs8);
                competitorDBContext.Add(cs9);
                competitorDBContext.Add(cs10);

                competitorDBContext.Add(t1);
                competitorDBContext.Add(t2);

                competitorDBContext.SaveChanges();
            }
        }

        public static void showCompetitors()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                var comps = competitorDBContext.Competitors
                    .Include(c => c.Coach)
                    .Include(c => c.Team)
                    .Include(c => c.ChampionSkills)
                    .ToList();

                var skills = competitorDBContext.Skills
                    .ToList();

                foreach (var competitor in comps)
                {
                    Console.WriteLine("Name: " + competitor.Name + Environment.NewLine + "Age: " + competitor.Age);
                    if (competitor.Coach != null)
                    {
                        Console.WriteLine("Coach: " + competitor.Coach.Name);
                    }

                    Console.WriteLine("Team: " + competitor.Team.TeamName);
                    Console.WriteLine("Skills: ");

                    foreach (ChampionSkill championSkills in competitor.ChampionSkills)
                    {
                        Console.WriteLine(championSkills.Champion.SkillName);
                    }
                    Console.WriteLine(Environment.NewLine);
                }

            }
        }
        public static void deleteCompetitors()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                var competitors = competitorDBContext.Competitors.Where(s => s.Id > 13);
                competitorDBContext.Competitors.RemoveRange(competitors);

                competitorDBContext.SaveChanges();
            }
        }

        public static void deleteAllfromDB()
        {
            using (CompetitorDBContext competitorDBContext = new CompetitorDBContext())
            {
                foreach (Team team in competitorDBContext.Teams)
                {
                    competitorDBContext.Remove(team);
                }
                foreach (Skill skill in competitorDBContext.Skills)
                {
                    competitorDBContext.Remove(skill);
                }

                competitorDBContext.SaveChanges();
            }
        }
    }
}
