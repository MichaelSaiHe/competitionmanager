﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompMan
{
    public class CompetitorDBContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=CompMan;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<ChampionSkill> ChampionSkills { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChampionSkill>().HasKey(cs => new { cs.CompetitorId, cs.ChampionId });
        }

    }
}
