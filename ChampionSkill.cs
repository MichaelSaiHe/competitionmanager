﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompMan
{
    public class ChampionSkill
    {
        [Key, Column(Order = 1)]
        public int CompetitorId { get; set; }
        [Key, Column(Order = 2)]
        public int ChampionId { get; set; }
        public Skill Champion { get; set; }
        public Competitor Competitor { get; set; }

        public ChampionSkill()
        {
        }

        public ChampionSkill(Competitor competitor, Skill skill)
        {
            this.Competitor = competitor;
            this.Champion = skill;
        }

    }
}
