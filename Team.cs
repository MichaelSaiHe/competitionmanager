﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompMan
{
    public class Team
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public List<Competitor> Competitors { get; set; }

        [ForeignKey("Coach")]
        public int CoachId { get; set; }
        public Coach Coach { get; set; }

        public Team()
        {
            Competitors = new List<Competitor>();
        }
        public Team(string teamName)
        {
            Competitors = new List<Competitor>();
            TeamName = teamName;
        }

    }
}

