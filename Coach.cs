﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CompMan
{
    public class Coach
    {
        [ForeignKey("Competitor")]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual Competitor Competitor { get; set; }
        public virtual Team Team { get; set; }

        public Coach()
        {
        }

        public Coach(string name, Team team)
        {
            Name = name;
            Team = team;
        }

    }
}
