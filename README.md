## Competition Manager
A console application that manages participants of a League of Legends competition.

### Classes that gets added to database: 
Competitor: contains information about the competitor. Competitors must belong to a team.

Coach: has a one to many relationship to competitors and one to one relationship with teams. Can have many competitors and one team.

Team: has a one to many relationship to Competitors and one to one relationship with Coaches. A team Can have amny competitors and one Coach. ¨

Skill: Proficiency on Champion. Has a many to many relationship to Competitors. 

ChampionSkill: a joining class between Competitor and Skills to handle the many to many relationship.