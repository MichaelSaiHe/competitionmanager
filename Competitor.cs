﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompMan
{
    public class Competitor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        [ForeignKey("Team")]
        public int TeamId { get; set; }

        public Team Team { get; set; }

        public virtual Coach Coach { get; set; }
        public string Position { get; set; }
        public List<ChampionSkill> ChampionSkills { get; set; }

        public Competitor()
        {
            ChampionSkills = new List<ChampionSkill>();
        }
        public Competitor(string name, int age, Team team, string position)
        {
            Name = name;
            Age = age;
            Team = team;
            Position = position;
            ChampionSkills = new List<ChampionSkill>();
        }
        public Competitor(int id, string name, int age)
        {
            Id = id;
            Name = name;
            Age = age;
            ChampionSkills = new List<ChampionSkill>();
        }
    }
}

