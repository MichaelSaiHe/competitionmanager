﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompMan
{
    public class Skill
    {
        public int Id { get; set; }
        public string SkillName { get; set; }
        public List<ChampionSkill> ChampionSkills { get; set; }

        public Skill()
        {
            ChampionSkills = new List<ChampionSkill>();
            this.SkillName = "";
        }

        public Skill(string name)
        {
            ChampionSkills = new List<ChampionSkill>();
            this.SkillName = name;
        }
    }
}
